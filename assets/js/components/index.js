import VFSites from "./vf-sites.js";
import VFSite from "./vf-site.js";

customElements.define("vf-sites", VFSites);
customElements.define("vf-site", VFSite);

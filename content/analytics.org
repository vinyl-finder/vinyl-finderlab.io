#+TITLE: Public Analytics
#+INCLUDE: ./templates/head-extra.org

#+ATTR_HTML: :text-center true
The [[file:../index.org][website]] visits [[https://analytics.eu.umami.is/share/xece1KrkTcpjzEfE/vinyl-finder][analytics]] are privacy respecting and public (using [[https://umami.is][umami]]).

#+INCLUDE: ./templates/footer.org

* COMMENT iframe not working
#+begin_export html
<iframe src="https://eu.umami.is/share/xece1KrkTcpjzEfE/vinyl-finder.gitlab.io"></iframe>
#+end_export


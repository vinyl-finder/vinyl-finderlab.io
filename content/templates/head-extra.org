#+HTML_HEAD_EXTRA: <meta name="description" content="Find vinyls online, in local record stores around the world" />
#+HTML_HEAD_EXTRA: <meta property="og:url" content="https://vinyl-finder.gitlab.io" />
#+HTML_HEAD_EXTRA: <meta property="og:type" content="website" />
#+HTML_HEAD_EXTRA: <meta property="og:title" content="Vinyl Finder" />
#+HTML_HEAD_EXTRA: <meta property="og:description" content="Find vinyls online, in local record stores around the world." />
#+HTML_HEAD_EXTRA: <meta property="og:image" content="/favicon.png" />
#+HTML_HEAD_EXTRA: <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Vinyl Finder, vinyl records search engine"/>
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="/css/index.css">
#+HTML_HEAD_EXTRA: <script type="module" src="/js/index.js"></script>
#+HTML_HEAD_EXTRA: <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7578240828852744" crossorigin="anonymous" ></script>
#+HTML_HEAD_EXTRA: <script async src="https://cse.google.com/cse.js?cx=010629519132760995870:yi4eyysyzr0" ></script>
#+HTML_HEAD_EXTRA: <script async src="https://eu.umami.is/script.js" data-website-id="0c19139a-e4c0-4c1b-a355-f6403ac4bed8"></script>

#+TITLE: New record store
#+INCLUDE: ../templates/head-extra.org

#+ATTR_HTML: :text-center true
Add a [[https://forms.gle/PjsgWGfzuAGVB3p99][new site]] to the [[file:index.org][list of record stores]] indexed by [[file:../index.org][vinyl-finder]].

#+begin_export html
<iframe
  src="https://docs.google.com/forms/d/e/1FAIpQLSeEwm8cAHl3rXbiFA76W7fP4StLkq0qEIPu3bt3lf0Jozflzg/viewform?embedded=true"
	frameborder="0"
	marginheight="0"
	marginwidth="0"
	>Loading…
</iframe>
#+end_export

#+begin_export html
<style>
body {
	padding: 0;
}
header {
	display: none;
}
p {
	margin: 0;
}
</style>
#+end_export
